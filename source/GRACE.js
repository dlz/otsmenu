
$ = $ || {};
SHOW_DEBUG = true;
(function($){
	var GRACE = function(){
		this._debugWindowName = null;

		this.is = function(o, t){
			return (typeof o === t);
		};
		this.isUndefined = function(o){
			return this.is(o, 'undefined');
		};
		this.isString = function(s){
			return this.is(s, 'string');
		};
		this.isObject = function(o){
			return this.is(o, 'object');
		};
		this.consoleLog = function(prefix, msg){
			var b = chrome.extension.getBackgroundPage();
			prefix = this.scrub(prefix, "");
			msg = ($.isArray(msg)) ? $.merge([prefix], msg) : [prefix, msg];
			b.console.log(msg);
			return (this == b);
		};
		this.Debug = function(msg){
			if(this.SHOW_DEBUG){
				return this.consoleLog("[==DEBUG==]", msg);
			}
		};
		this.Error = function(msg){
			return this.consoleLog("[==ERROR==]", msg);
		};
		this.voidFunction = function(response){
			return;
		};
		this.serializeXML = function(xml){
			var serial = new XMLSerializer();
			return serial.serializeToString(xml);
		};
		this.scrub = function(o, val){
			val = val || ((typeof val == "string") ? "" : {});
			o = o || val;
			return o;
		};
		this.mapScrub = function(o, map){
			for(i in map){
				if(this.is(o, i)){
					return map[i];
				}
			}
			return o;
		};
		this.translate = function(s, map){
			for(i in map){
				if(s == i){
					return map[i];
				}
			}
			return s;
		}
		this.BackgroundConsoleLog = function(msg){
			var b = chrome.extension.getBackgroundPage();
			b.console.log(msg);
			return (this == b);
		}



		this.SHOW_DEBUG = SHOW_DEBUG || false;

		this.Message = function(type){

			this.STATUS_CODE = {
				UNSET: null,
				NULL: null,
				UNDEFINED: "UNDEFINED",
				FAIL: -100,
				FALSE: 0,		
				TRUE: 1,
				SUCCESS: 100
			};

			this.MESSAGE_TYPE = {
				UNSET: null,
				GET: 'GET',
				SET: 'SET',
				ENABLE: 'ENABLE',
				DISABLE: 'DISABLE'
			};

			this._type = this.MESSAGE_TYPE.UNSET;
			this._title = "";
			this._payload = {};
			this._status = this.STATUS_CODE.UNSET;

			this.setType = function(newtype){
				newtype = this.scrub(newtype, this.MESSAGE_TYPE.UNSET);
				this._type = newtype;
				return this._type;
			};
			this.setTitle = function(newtitle){
				this._title = this.scrub(newtitle, "");
				return this._title;
			};
			this.setPayload = function(index, value){
				index = this.scrub(index, {});
				var result = {};
				if(this.isUndefined(value)){
					this._payload = $.extend(this._payload, index);
					// On return, we can check if result is an array.
					// If so, then its first slot is the whole payload.
					result = [this._payload];
				} else {
					if(this.isString(index)){
						this._payload[index] = value;
						result[index] = value;
					} else {
						// We've passed a value but index is not a string.
						// So we're going to ignore index and append value to the end of the payload.
						var len = this._payload.length;
						this._payload[len] = value;
						result[len] = value;
					}
				}
				return result;
			};
			this.replacePayload = function(newpayload){
				this._payload = this.scrub(newpayload, {});
			};
			this.emptyPayload = function(){
				this._payload = {};
			};
			this.setStatus = function(status_code){
				this._status = this.scrub(status_code, this.STATUS_CODE.UNSET);
			};

			this.getType = function(){
				return this._type;
			};
			this.getTitle = function(){
				return this._title;
			};
			this.getPayload = function(index){
				if(this.isUndefined(index)){
					return this._payload;
				} else {
					if(this.isUndefined(this._payload[index])){
						return null;
					} else {
						return this._payload[index];
					}
				}
			};
			this.getStatus = function(){
				return this._status;
			};


			// Constructor
			type = (this.isUndefined(type)) ? "" : type;
			if(this.isString(type)){
				this._type = this.scrub(type, this.MESSAGE_TYPE.UNSET);
			} else if(this.isObject(type)){
				if(type.constructor.name === "Object"){
					for(i in this){
						if(i.substr(0,1) == '_'){
							var prop = i.substring(1);
							if(!this.isUndefined(type[prop])){
								this[i] = this.scrub(type[prop], this[i]);
							}
						}
					}
	/*
					if(!$.isUndefined(type.type)){
						this._type = $.scrub(type.type, this._type);
					}
					if(!$.isUndefined(type.title)){
						this._title = $.scrub(type.title, this._title);
					}
					if(!$.isUndefined(type.payload)){
						this._payload = $.scrub(type.payload, this._payload);
					}
					if(!$.isUndefined(type.status)){
						this._status = $.scrub(type.status, this._status);
					}
	*/				
				}
			}
		};

		this.registerDebugName = function(windowname){
			var _debug = this.Debug;
			this.Debug = function(msg){
				var debugId = "[--" + windowname + "--]";
				msg = ($.isArray(msg)) ? $.merge([debugId], msg) : [debugId, msg];
				var m = [msg];
				_debug.apply(this, m);
			};
		}

		this.sendEnablePopupMessage = function(popuphtml){
			var msg = new this.Message("ENABLE");
			msg.message_title = "POPUP";
			msg.payload = [popuphtml];
			chrome.extension.sendRequest(msg, this.voidFunction);
		};

		this.sendSetMessage = function(field, value){
			var msg = new this.Message("SET");
			msg.message_title = field;
			msg.payload = [value];
			chrome.extension.sendRequest(msg, this.voidFunction);
		};

		this.sendDataRequest = function(data, tab_id, callback){
			var msg = new this.Message("GET");
			msg.message_title = data;
			msg.payload = [{tabId: tab_id}];
			chrome.extension.sendRequest(msg, callback);
		};

		this.enablePageActionPopup = function(popuphtml){
			this.sendEnablePopupMessage(popuphtml);
		};
	};

	window._G  = new GRACE();
	_G.Debug(["[--GRACE--]", "Loaded GRACE.js."]);
})(jQuery);