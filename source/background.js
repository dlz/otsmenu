var _G = _G || {};
window.name = "OT&S Magic Menu Background";
_G.registerDebugName(window.name);
_G.Debug("Proccessing...");

window._settings = {};
window._settings.link_icon = "./external_link_icon.gif";
window._settings.xml_url = "./menu.xml";

window.extensionData = [];
window.extensionData["GLOBAL"] = {};
window.extensionData["GLOBAL"].links_xml = "";
window.extensionData["GLOBAL"].menu = {};

if(!window.extensionData["GLOBAL"].links_xml.length){
	//_G.Debug(["$.get",fetchXMLData()]);
	fetchXMLData().then(function(){
		buildMenu();
	});
}

function fetchXMLData(){
	return $.get(window._settings.xml_url, function(xml){
		window.extensionData["GLOBAL"].links_xml = xml;
	});
}

function buildMenu(){
	_G.Debug("Building Menu Object");
	var menu = {};
	var $xml = $(window.extensionData["GLOBAL"].links_xml);
	menu.version = $($xml.find("extension_version")[0]).text();
	menu.linkset = $($xml.find("menu_set")[0]).text();
	menu.title = $($xml.find("menu_name")[0]).text();
	menu.groups = [];
	$xml.find("group").each(function(i, ele){
		var group = {name: $(ele).attr("name"), objects: []};
		$(ele).children().each(function(j, obj){
			var o = {};
			switch($(obj)[0].tagName){
				case "link":
					o = {
						type: 'link',
						icon: window._settings.link_icon,
						href: $(obj).attr("url"),
						text: obj.textContent,
						getLI: function(){
							return $("<li><a href='" + this.href + "' target='_blank'><img src='" + this.icon + "' />" + this.text + "</a></li>");
						}
					};
				break;
				case "separator":
					o = {
						type: 'hr',
						getLI: function(){
							return $("<li><hr /></li>");
						}
					};
				break;
				default:
				break;
			}
			group.objects.push(o);
		});
		menu.groups.push(group);
	});
	window.extensionData["GLOBAL"].menu = menu;
}