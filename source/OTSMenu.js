/*
	File: OTSMenu.js
	Version: 0.1.3
*/
var _G = _G || {};
window.name = "OT&S Magic Menu Popup";
_G.registerDebugName(window.name);
_G.Debug("Proccessing...");
//_G.Debug("And on the eigth day, God created GRACE. And he saw that it was good.");

$(function(){
	buildPopup();
});

function buildPopup(){
	var body = $($("body")[0]);
	var menu = chrome.extension.getBackgroundPage().extensionData['GLOBAL'].menu;
	var content_container = $("<div id='tab-content-container'></div>");
	var tabs = $("<div id='tabs'></div>");
	var tabsul = $("<ul></ul>");

	body.attr("tabindex", "1");	
	body.append($("<h1></h1>").append(menu.title));
	$(menu.groups).each(function(i,ele){
		// Build Tab
		var tab = $("<li class='tab-button'></li>");
		var hotspot = $("<a href='#'></a>").append(ele.name);
		hotspot.click(function(e){
			e.preventDefault();
			openTab(i);
		});
		tab.append(hotspot);
		tabsul.append(tab);

		// Build Content
		var content = $("<div class='tab-content'></div>");
		var list = $("<ul></ul>");
		content.append($("<h2></h2>").append(ele.name));
		$(ele.objects).each(function(){
			list.append(this.getLI());
		});
		content_container.append(content.append(list));
	});
	tabs.append(tabsul);	

	// Build Version
	var version = $("<div id='version'></div>").append(menu.version + "::" + menu.linkset);
	tabs.append(version);

	// Add our content and tabs to the body
	body.append(content_container);
	body.append(tabs);
	body.find("a").each(function(i,ele){ele.blur();});

	// Build Status Bar
	var statusbar = $("<div id='statusbar'>&nbsp;</div>");
	body.on("mouseover",".tab-content a",function(event){statusbar.text(this.href);});
	body.on("mouseout",".tab-content a", function(event){statusbar.text("");});
	body.append(statusbar);

	// Open Most Recent Tab
	var recentTab = localStorage.getItem("defaultTab") || 0;
	openTab(recentTab);

	// Reset Focus
	body.focus();
}


function openTab(tabId){
	// Close All Open Tabs
	$("div.selected").each(function(){$(this).hide();});
	$("li.selected").each(function(){$(this).removeClass("selected");});

	$($(".tab-content")[tabId]).addClass("selected");
	$($("#tabs ul li")[tabId]).addClass("selected");
	$($(".tab-content")[tabId]).show();	
	localStorage.setItem("defaultTab", tabId);
}
